from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings
from django.views.generic import RedirectView

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^blog/comments/', include('fluent_comments.urls')),
    url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    url(r'^$', RedirectView.as_view(url= '/ui/'), name='ui home'),
    url(r'^ajax-upload/', include('ajax_upload.urls')),
    url(r'^accounts/login/$', 'django.contrib.auth.views.login', {'template_name': 'accounts/login.html'}),
    url(r'^accounts/logout/$', 'django.contrib.auth.views.logout', {'template_name': 'accounts/logout.html'}),
    url(r'^accounts/', include('registration_email.backends.default.urls')),
    url(r'^admin/', include(admin.site.urls)),
    # FIXME ICOTOI: Redirectionez dupa login catre projects mai degraba decat catre profilul utilizator
    url(r'^accounts/profile/$', RedirectView.as_view (url= '/')),
    url(r'^users/(?P<user>.*)/$', RedirectView.as_view (url= '/')),  # TODO icotoi redirect to profile fill in
    url(r'^ui/', include('ui.urls')),
    url(r'^api/', include('api.urls')),
    url(r'^messages/', include('postman.urls'))
)
