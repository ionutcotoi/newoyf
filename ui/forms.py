from api.models import Post
from django import forms
from ajax_upload.widgets import AjaxClearableFileInput

class AddPostForm(forms.ModelForm):
    class Meta:
        model = Post
        widgets = {
            'name'        : forms.TextInput(attrs={'placeholder': 'Title'}),
            'picture'     : AjaxClearableFileInput,
            'tags'        : forms.TextInput(attrs={'placeholder': 'Tags'}),
#            'description' : forms.Textarea(attrs={'placeholder': 'Description'})
        }
        exclude = ('user', 'description', 'picture_home') # These are filled in the view

    # def clean_name(self):
    #     super(AddProjectForm, self).clean()
    #     cleaned_data = self.cleaned_data
    #     nameField = cleaned_data.get('name', None)
    #     if ' ' in nameField:
    #         raise forms.ValidationError(u'Project Name cannot contain spaces')
    #         return nameField
    #     else:
    #         return nameField