from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django.shortcuts import render_to_response, render, get_object_or_404
from django.template.context import RequestContext
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserChangeForm
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
import uuid
from forms import AddPostForm
from api.models import Post


# Home page
def dashboard(request):
    return render(request, 'ui/home.html', {
        'page_title': "OFFYOURFACE | Home",
        })


def events(request):
    return render_to_response('ui/events.html', RequestContext(request))


def shuffle(request):
    return render_to_response('ui/shuffle.html', RequestContext(request))


def addpost(request):
    if request.method == 'POST' and request.user is not None: # If the form has been submitted...
        form = AddPostForm(request.POST, request.FILES ) # A form bound to the POST data
        if form.is_valid():
            newPost = form.save(commit = False)
            newPost.accessToken = str(uuid.uuid4())
            newPost.user = request.user
            newPost.save()
            # Save tags
            form.save_m2m()

            return HttpResponseRedirect('/') # Redirect after POST
    else:
        form = AddPostForm()
    return render(request, 'ui/addpost.html', {
        'form': form,
        })


def addevent(request):
    return render_to_response('ui/addevent.html', RequestContext(request))


def post(request, postId):
    thisPost = get_object_or_404(Post, pk=postId)
    thisPostTags = thisPost.tags.all()
    related = thisPost.tags.similar_objects()
    return render(request, 'ui/post.html', {
        'page_title': "OFFYOURFACE | " + thisPost.name,
        'post': thisPost,
        'tags': thisPostTags,
        'related_posts': related,
        })