from django.db import models
from django.contrib.auth.models import User 
#from django.contrib.gis.db import models
from taggit.managers import TaggableManager
from imagekit.models import ImageSpecField
from imagekit.processors import ResizeToFill, SmartResize, ResizeToFit

# Create your models here.
#class dataType(models.Model):
#    name = models.CharField(max_length = 25, default="Integer")
#    storedType = models.CharField(max_length = 25, default="IntegerField")

class Post(models.Model):
    user = models.ForeignKey(User)
    name = models.CharField(max_length = 25)
    description = models.TextField(max_length = 250, default="", blank=True)
#    accessToken = models.CharField(max_length = 40, default="")
#    public = models.NullBooleanField(default=False, null=True)
    picture = models.FileField(upload_to='user/posts/%Y/%m/%d', 
                               default="", 
                               help_text="max. 5MB", 
                               verbose_name="Select an image file", 
                               blank=False)
    picture_home = ImageSpecField(source='picture',
                                      processors=[ResizeToFit(368, 600)],
                                      format='JPEG',
                                      options={'quality': 60})
    tags = TaggableManager()
    timestamp = models.DateTimeField(auto_now_add = True)
#    location = models.PointField(null=True)
#    objects  = models.GeoManager()
# Create your models here.
