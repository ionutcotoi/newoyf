from django.conf.urls import patterns, url
from api import views
from django.views.generic import RedirectView

urlpatterns = patterns('',
    # url(r'^$', views.dashboard, name='dashboard'),
    url(r'^posts$', views.posts, name='posts'),
    # url(r'^shuffle$', views.shuffle, name='shuffle'),
    #url(r'^project/(?P<projectId>\d+)/$', views.project, name='project'),
    #url(r'^addproject$', views.addproject, name='addproject'),
    #url(r'^project/(?P<projectId>\d+)/edit$', views.editproject, name='editproject'),
    #url(r'^project/(?P<projectId>\d+)/sensor/(?P<sensorId>\d+)/$', views.sensor, name='sensor'),
    #url(r'^project/(?P<projectId>\d+)/addsensor$', views.addsensor, name='addsensor'),
    #url(r'^project/(?P<projectId>\d+)/sensor/(?P<sensorId>\d+)/edit$', views.editsensor, name='editsensor'),
    #url(r'^profile/(?P<userName>[a-zA-Z0-9_.-]+)$', views.profile, name='profile'),
    #url(r'^profile/(?P<userName>[a-zA-Z0-9_.-]+)/edit$', views.editprofile, name='editprofile'),
    #url(r'^about$', views.about, name='about'),
    #url(r'^contact$', views.contact.as_view(), name='contact'),
    #url(r'^thankyou$', direct_to_template, {'template': 'frontend/thankyou.html'}, name='thankyou')
)
