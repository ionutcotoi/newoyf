from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django.shortcuts import render_to_response, render, get_object_or_404
from django.template.context import RequestContext
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserChangeForm
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User
from django.core import serializers
import json
from models import Post

# Home page
def posts(request):
    """
    Render posts in db based on filter in GET
    :param request:
    :return:
    """
    if request.method == 'GET':
        postItems = Post.objects.all()
        print request.GET
        if 'filter' in request.GET:
            if request.GET['filter'] == 'tag':
                if 'tag' in request.GET:
                    postItems = postItems.filter(tags__name__in=[request.GET['tag']])
            if request.GET['filter'] == 'user':
                if 'user' in request.GET:
                    postItems = postItems.filter(user__username__in=[request.GET['user']])

        jsonItems=[]
        for item in postItems:
            jsonItem = {}
            jsonItem['id'] = item.pk
            jsonItem['title'] = item.name
            jsonItem['picture'] = item.picture.url
            jsonItem['home_picture'] = item.picture_home.url
            jsonItems.append(jsonItem)

        return HttpResponse(json.dumps(jsonItems, indent=2), content_type="application/json")
    else:
        return HttpResponse("ERROR: Invalid request type!", content_type="text/plain")

