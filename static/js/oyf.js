(function($){
  $.oyf = function(options, cb) {
//    var queryStr = window.location.search.substr(1).split("?");
//    if(queryStr != "") {
//        $.oyf.defaults['api'] = $.oyf.defaults['api'] + queryStr;
//    }
    var posts = [], o = $.extend({}, $.oyf.defaults, options);

    $.getJSON(o.api, function(result){

      $.each(result, function(i, post){
        // Create an absolute permalink containing the api url.
        //post.data.permalink_absolute = o.api + post.data.permalink;
        //post.fields.id = post.id;
        post.permalink ="/ui/post/" + post.id;
        // Append to posts array.
        posts.push(post);
      });
      cb.call(self, posts);
    });
  };

  $.oyf.defaults = {
    api: '/api/posts' + "?"+window.location.search.substr(1).split("?"),
    limit: 20
  };
})(jQuery);
