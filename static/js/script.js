$(function(){

  // Fire events on specific media query layout changes.
  mql('all and (max-width: 480px)', reLayout);
  mql('all and (max-width: 768px)', reLayout);
  mql('all and (min-width: 980px)', reLayout);
  mql('all and (min-width: 1200px)', reLayout);

  var $posts = $('#posts');

  $(window).hashchange(function(){
    load(location.hash.replace('#', ''), 50);
  });

  function load(limit) {
    clear();

    $.oyf({ limit: limit }, function(data){

      // Append data to posts using the #post template.
      $('#post').tmpl(data).appendTo($posts);

      // Isotopize the posts.
      $posts.isotope({
        itemSelector : '.item'
      });

      // Look for images and append to posts after isotope has initiated
      setTimeout(function(){
        $.each(data, function(i, post){
        //  $('<img class="image">').attr('src', '/media/' + post.picture).wrap('<div class="image">').load(function(){
        //    $(this).appendTo('#postid-' + post.id + ' div.well').slideDown(function(){
              $posts.isotope('reLayout');
        //    });
        //  });
        });
      }, 500);

    });
  }

  function clear() {
    $posts.isotope('destroy').empty();
  }

  function reLayout(mql) {
    $posts.isotope('reLayout');
  }

  // Load front page of oyf on load.
  load(location.hash.replace('#', ''), 50);

});
